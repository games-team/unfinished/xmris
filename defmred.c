/* Copyright (C) 1993, 1992 Nathan Sidwell */
/* RCS $Id: defmred.c 1.2 Tue, 16 Mar 1999 11:28:16 +0000 nathan $ */
#define EXTERN
#include "xmred.h"
#include "defmred.h"
#include "defcom.c"
/*{{{  BOARD initial_board[] =*/
BOARD initial_board[] =
{
#include "xmred.gdn"
};
/*}}}*/
