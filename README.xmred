


Games and Demos                                          XMRED(6)



NNNNAAAAMMMMEEEE
     xmred - garden editor for xmris

SSSSYYYYNNNNOOOOPPPPSSSSIIIISSSS
     xxxxmmmmrrrreeeedddd [-option ...] [-toolkitoption ...]

DDDDEEEESSSSCCCCRRRRIIIIPPPPTTTTIIIIOOOONNNN
     Mr Ed is a tool for creating and editing  alternate  gardens
     for Mr Is.  Hopefully its user interface is intuitive enough
     that most of this manual page need not be read by  a  casual
     user.

     In the default configuration, the  main  window  presents  a
     garden image on the left, a control panel on the right and a
     list of gardens underneath. Gardens can be loaded and  saved
     using  the  menu,  these then appear in the list below.  The
     insert option loads a file into the copy buffer. It can then
     be dragged to the desired place in the garden list.

     To edit a garden (or comment, or include), either select  it
     with  a  button  press  and release, or with a drag and drop
     into the edit box at the lower right. The drag and drop will
     remove  it from the garden list, selection will copy it, and
     the source garden will be updated too.  To  delete  gardens,
     drag  them to the copy buffer.  The main garden display will
     be updated, if it was a garden being moved to the edit  box.
     The  box  on  the lower right is a copy buffer. You can drag
     and drop into and out of this to the garden list or  one  of
     the  edit  boxes.  Similarly  you can drag and drop from the
     edit boxes to the garden list, and within  the  garden  list
     itself.  Note  that drag and drop always removes the source,
     if it is within the garden list, whereas selection does  not
     delete the source.

     The control panel shows a set of options, which can be bound
     to  buttons.  This  can  be done either by press and release
     with the required button, or drag and drop onto the required
     button's  icon. You can bind an option to more than one but-
     ton. However, press and release on the  apples  option  will
     simply  change  the  currently  selected explicit apple, not
     necessarily bind that option to a new button. The fill  pat-
     tern  and  color  can  be  select  by  pressing on them. The
     current garden will be updated. The list  of  buttons  shows
     which  options  are  bound  to  which  buttons. You can also
     change button bindings by pressing  the  destination  button
     with  the pointer over the button with the required option's
     icon, or by drag and drop from button to button.

     The six options are,

     AAAApppppppplllleeee
          This selects where apples from one of the four sets  of



X Version 11      Last change: 20 December 1993                 1






Games and Demos                                          XMRED(6)



          explicit  apple  locations  are placed. To select which
          set is being controlled, press on the relevant quadrant
          of  the  icon.  One of these sets is used when Mr Is is
          used with the +random option. You can place apples any-
          where there isn't a path, even in an unstable location,
          which will immediately fall. Placing an explicit  apple
          on a path, will fill the path.

     RRRRaaaannnnddddoooommmm
          This controls where apples may be placed  randomly,  if
          Mr  Is  is started with the -random option. Apples will
          only be placed on the  specified  locations,  or  where
          ever  an  explicit  apple  could be located. Unless you
          specify otherwise, random apples may  be  located  any-
          where there isn't a path, even unstable locations which
          will immediately fall. Placing  a  random  apple  on  a
          path, will fill the path.

     CCCChhhheeeerrrrrrrryyyy
          This controls where cherries are located. There must be
          at  least  one  cherry  on  a  garden.  Cherries may be
          located anywhere on the board.

     PPPPaaaatttthhhh This controls where the initial path is. Each  location
          on  the  board  consists  of  a  centre and four edges.
          Depending on the exact location  of  the  pointer,  you
          will  select  either  the centre, or an edge. Filling a
          path will place a random apple in the filled  location.
          The four locations immediately below the

     PPPPllllaaaayyyyeeeerrrr
          This controls where the player starts from. There  must
          be exactly one such location.

     DDDDeeeennnn  This controls where the monsters start from. There must
          be at least one den.

     The list of totals show the counts for the explicit  apples,
     cherries,  random  apples  required,  apple spaces, unstable
     apple positions, dens and player  positions.  A  warning  is
     shown  on  the left of any which are inconsistant, or out of
     range. The number of apples for a garden can be set by  mov-
     ing  the scroll bar at the left of the apple icon. Note that
     when you change the number  of  apples,  or  add  or  remove
     explicit  apples, a warning will change on on some or all of
     the explicit apple counts. This is just to remind  you  that
     you  must do some more work on the garden, before it is con-
     sistant. There are two types of random apple spaces,  stable
     and  unstable.  The  unstable  space  count is shown with an
     arrow in it, the other apple space  count  shows  the  total
     number  of apple spaces. The hazzard warning on the unstable
     count,  just  shows  that  you  have  some  unstable   apple



X Version 11      Last change: 20 December 1993                 2






Games and Demos                                          XMRED(6)



     positions  (this  may be intentional on your part). The haz-
     zard on the total apple space count indicates that there are
     less  spaces than the number of apples you specified for the
     garden.

     Below this is a comment box for the garden.  Selecting  this
     will  pop  up a dialog which you can enter information about
     the garden.

     At the lower right of the control panel is  a  display  mode
     selector. There are three display modes. The first shows all
     the explicit apple positions, 4 to a cell when required. The
     second  shows  only  one  set  of  explicit  apples, the set
     selected using the apples option quadrant.  The  final  mode
     shows  none  of  the  explicit apples, just the random apple
     spaces.

     The garden display is on the left  of  this.  It  shows  the
     currently edited garden. Clicking or dragging a mouse button
     on this area will perform the option currently bound to that
     button. You will notice the totals change when you do this.

     Mr Ed will use colour sprites, if the visual permits it (has
     a  colour  map  size of more than 15, and you haven't forced
     monochrome). All the colours bar black and  white  are  user
     definable.  There  are  four  sets, one for each of the four
     combinations of gender and swap flag. The colours are  allo-
     cated  in  reverse  order of their distance in colour space,
     from currently allocated colours (the most  distant  colours
     are  allocated first). That way, if allocation fails because
     the colour map is  full,  an  allocated  substitute  colour,
     which  is  nearest  the  desired colour, can be used and the
     allocated colours are kept maximally distant.  You can limit
     the number of distinct colours with the -distinct option.  A
     warning message is sent to stderr, if  a  colour  allocation
     fails.  The -colours argument shows how these are allocated,
     and -help -colours can be used to get  the  colour  resource
     names.

OOOOPPPPTTTTIIIIOOOONNNNSSSS
     Mr Ed accepts the standard X Toolkit  options,  as  well  as
     these.

     -hhhheeeellllpppp
          Lists the command options, application resource  fields
          and  some  other  information to stderr. Does not start
          the game. If the -colours option is supplied too,  then
          the  colour  resource  classes are listed instead, with
          their default values. The format of this list is  suit-
          able  for inclusion in a resource file. Note, this does
          not list out the colour values that you  would  get  if
          you  ran  the  game,  as  it  does  not  read the color



X Version 11      Last change: 20 December 1993                 3






Games and Demos                                          XMRED(6)



          resources.

     -sssswwwwaaaapppp
     -rrrrvvvv
     -rrrreeeevvvveeeerrrrsssseeee
          Normally the foreground is  black  and  the  background
          white,  this  swaps them round. On colour systems, this
          may also alter other colours.

     ++++sssswwwwaaaapppp
     -nnnnoooosssswwwwaaaapppp
          Override a swap resource in your  resources,  to  force
          unswapped colours.

     -mmmmoooonnnnoooo
          Use black and white, even on colour  displays.  (Unfor-
          tunately,  the obvious option, '-bw', is already nabbed
          by the toolkit as borderwidth.)

     -mmmmrrrriiiissss
     -mmmmssssiiiitttt
     -ggggeeeennnnddddeeeerrrr _g_e_n_d_e_r
          Mr Ed has two sets of sprites. Mris selects the classic
          sprites,  while  msit  selects a more modern set. Valid
          genders are  'he',  'she',  'female',  'male',  'msit',
          'mris', 'boy', 'girl'.

     -ddddeeeepppptttthhhh _d_e_p_t_h
          Mr Ed will use the default depth of the screen. You may
          wish to override that by using this option. Selecting a
          different depth may affect the visual selected.

     -vvvviiiissssuuuuaaaallll _v_i_s_u_a_l-_c_l_a_s_s
          Mr Ed will pick the default visual, but you  can  over-
          ride  that  by  specifying  a  particular visual class.
          Valid   visuals   are   'PseudoColor',   'DirectColor',
          'TrueColor',   'StaticColor',  'GrayScale',  and  'Sta-
          ticGray'. To see which one is picked, you can  use  the
          -colours option. If you do select a non-default visual,
          you may have to specify a private colour map  too,  due
          to limitations of the server or display.

     -pppprrrriiiivvvvaaaatttteeee
          This forces Mr Ed to allocate  a  private  colour  map.
          Normally Mr Ed will share the default colour map of the
          selected visual, but if that does not have enough  free
          colour cells then some colours will have to be shared.

     -ccccoooolllloooouuuurrrrssss
     -ccccoooolllloooorrrrssss
          Show how the colours are allocated,  and  which  visual
          has been selected.  The allocation is listed to stdout.



X Version 11      Last change: 20 December 1993                 4






Games and Demos                                          XMRED(6)



          When allocating each colour, its resource name and  rgb
          values  are  listed  together  with the nearest already
          allocated colour  and  the  distance  between  them  in
          colour  space.  The  allocated  pixel number is printed
          last. If  given  with  the  -help  option,  the  colour
          resource  classes  are  listed,  and  the game does not
          start.

     -ddddiiiissssttttiiiinnnncccctttt _n
          Sets the number of distinct colours used. This  can  be
          used  to  limit  the  number  of  colours used from the
          colour map. Black and white are not included, and  nei-
          ther  are  the two writable colours used for the garden
          backgrounds on dynamic visuals. Note that  -distinct  0
          is different from -mono, even though both will only use
          black and white.

RRRREEEESSSSOOOOUUUURRRRCCCCEEEESSSS
     Mr Ed uses the X toolkit application resource mechanism  for
     setting up the environment. Application resource items start
     with 'Xmris', so that Mr Ed will pick up your  defaults  for
     Mr  Is.  The  resource  name  can  be derived from the given
     resource class by decapitalizing it. For  example  'cherryS-
     talk'  is the resource name for the class 'cherryStalk'. The
     following classes are used (choices in '{}' and defaults  in
     '[]'.)

     XXXXmmmmrrrriiiissss....RRRReeeevvvveeeerrrrsssseeeeVVVViiiiddddeeeeoooo:::: {_y_e_s, _n_o} [[[[nnnnoooo]]]]
          Specifies whether to use swapped colours or not.

     XXXXmmmmrrrriiiissss....MMMMoooonnnnoooo:::: {_y_e_s, _n_o} [[[[nnnnoooo]]]]
          Whether  the  default  is  for  monochrome  on   colour
          displays.

     XXXXmmmmrrrriiiissss....GGGGeeeennnnddddeeeerrrr:::: _g_e_n_d_e_r [[[[hhhheeee]]]]
          Sets the default game gender. Valid genders are 'mris',
          'msit', 'she', 'he', 'female', 'male', 'boy', 'girl'.

     XXXXmmmmrrrriiiissss....DDDDeeeepppptttthhhh:::: _d_e_p_t_h
          Set the required screen depth to use.

     XXXXmmmmrrrriiiissss....VVVViiiissssuuuuaaaallll:::: _v_i_s_u_a_l-_c_l_a_s_s
          Set the required visual class to use. Valid visuals are
          'PseudoColor',    'DirectColor',   'TrueColor',   'Sta-
          ticColor', 'GrayScale', and 'StaticGray'.

     XXXXmmmmrrrriiiissss....PPPPrrrriiiivvvvaaaatttteeee:::: {_y_e_s, _n_o} [[[[nnnnoooo]]]]
          Set whether or not to use a private colour map.

     XXXXmmmmrrrriiiissss....DDDDiiiissssttttiiiinnnncccctttt:::: _n
          Set the number of distinct colours allocated  from  the
          colour map.



X Version 11      Last change: 20 December 1993                 5






Games and Demos                                          XMRED(6)



     In addition, you have the normal resources such as '*Font'.

CCCCOOOOLLLLOOOOUUUURRRR RRRREEEESSSSOOOOUUUURRRRCCCCEEEESSSS
     There are many colour name defaults. For a full  description
     see  the  xmris(6)  manual  page,  but note that not all the
     colors are used for Mr Ed. Provided that you  specified  the
     colour  resources  for Mr Is loosely enough, Mr Ed will pick
     up the same overrides. The foreground  color  for  the  Icon
     widgets  is copied from the apple faint color on color visu-
     als (this is important for the noswap color scheme).

WWWWIIIIDDDDGGGGEEEETTTT RRRREEEESSSSOOOOUUUURRRRCCCCEEEESSSS
     There are a few resources which are picked up  from  widgets
     within  the  widget  tree. They are the initial button bind-
     ings, colors, fills and mode.  The bindings are attached  to
     the  individual  button  displays.  The options are 'apple',
     'random', 'cherry', 'path', 'player' and 'den'. The  colors,
     fills and mode are attached to the color, fill and mode form
     widgets. The value must be an integer in the correct range.

     There is an additional composite resource  for  children  of
     composite  widgets  (Paned and SimpleMenu), called 'attach'.
     This allows you to change the ordering of  sibling  widgets.
     Mr  Ed  uses this resource to determine the correct order to
     create the sibling widgets. For instance, to get the control
     panel  on  the left of the garden widget, use the constraint
     '*one.garden.attach:panel'.  In addition,  the  widgets  are
     created  in  such an order that Form constraints 'fromHoriz'
     and 'fromVert' can be specified in any order.

     There are four new widgets used for Mr Ed,  'Icon',  'Drag',
     'Garden' and 'PixmapList'.

     The Icon widget is a subclass of Simple. It displays a  pix-
     map  and  allows  its  selection with any button press. If a
     button is dragged on it, it may invoke a drag widget to per-
     form  a  drag operation.  It has the following new resources
     of interest.

     ddddrrrraaaaggggSSSSeeeennnnssssiiiittttiiiivvvviiiittttyyyy:::: _p_i_x_e_l_s [[[[4444]]]]
          Sets the minimum drag which must occur, before  a  drag
          widget  is  popped  up, to take over the dragging. When
          set to zero, dragging is disabled.

     ffffllllaaaasssshhhhDDDDeeeellllaaaayyyy:::: _d_e_l_a_y [[[[111100000000]]]]
          Sets the time for which the  widget  is  highlit  after
          selection. This time is in milliseconds.

     hhhhiiiigggghhhhlllliiiigggghhhhttttTTTThhhhiiiicccckkkknnnneeeessssssss:::: _p_i_x_e_l_s [[[[1111]]]]
          Sets the thickness of the highlight box.





X Version 11      Last change: 20 December 1993                 6






Games and Demos                                          XMRED(6)



     ffffoooorrrreeeeggggrrrroooouuuunnnndddd:::: _c_o_l_o_r [[[[ddddeeeeffffaaaauuuulllltttt ffffoooorrrreeeeggggrrrroooouuuunnnndddd]]]]
          Sets the color of the highlight box and block.

     The Drag widget is a subclass of OverrideShell. It  displays
     a  pixmap,  and  follows  the  pointer  until  a  button  is
     released. Its use is for drag and drop. It has no additional
     resources of user interest.

     The Garden widget is a subclass of Simple. It  performs  the
     garden  editing.   It  has  no  additional resources of user
     interest.

     The PixmapList widget is a subclass of Simple. It displays a
     list  of pixmaps, and permits them to be scrolled. Each pix-
     map may be selected by a button  press  on  it,  or  a  drag
     widget  invoked by dragging on a pixmap.  In addition to the
     Icon widget additional resources, it has the following addi-
     tional resources of user interest.

     ddddiiiissssttttaaaannnncccceeee:::: _p_i_x_e_l_s [[[[4444]]]]
          The distance between each pixmap.

     ppppiiiixxxxmmmmaaaappppBBBBoooorrrrddddeeeerrrrWWWWiiiiddddtttthhhh:::: _p_i_x_e_l_s [[[[1111]]]]
          The border width for each pixmap.

     ppppiiiixxxxmmmmaaaappppBBBBoooorrrrddddeeeerrrrCCCCoooolllloooorrrr:::: _c_o_l_o_r [[[[ddddeeeeffffaaaauuuulllltttt ffffoooorrrreeeeggggrrrroooouuuunnnndddd]]]]
          The border color for each pixmap.

EEEENNNNVVVVIIIIRRRROOOONNNNMMMMEEEENNNNTTTT
     A few environment variables are used to locate resources.

     DDDDIIIISSSSPPPPLLLLAAAAYYYY
         The default display to connect  to.  May  be  overridden
         with the -display option.

FFFFIIIILLLLEEEESSSS
     ~~~~////....XXXXddddeeeeffffaaaauuuullllttttssss
     ............////aaaapppppppp----ddddeeeeffffaaaauuuullllttttssss////XXXXmmmmrrrriiiissss....aaaadddd
     ............////aaaapppppppp----ddddeeeeffffaaaauuuullllttttssss////XXXXmmmmrrrriiiissss----ccccoooolllloooorrrr....aaaadddd
          You can place you favourite key bindings and  stuff  in
          an  application resource file, and Mr Ed will use them,
          rather than its compiled defaults.  See X for  informa-
          tion about how these are searched.

     ............////aaaapppppppp----ddddeeeeffffaaaauuuullllttttssss////xxxxmmmmrrrriiiissss////<<<<ggggaaaarrrrddddeeeennnnssss>>>>
          Search path for loadable gardens used by Mr Is.

SSSSEEEEEEEE AAAALLLLSSSSOOOO
     xxxxmmmmrrrriiiissss(6)

EEEERRRRRRRROOOORRRRSSSS




X Version 11      Last change: 20 December 1993                 7






Games and Demos                                          XMRED(6)



     If  a  loaded  garden  is  incorrect,  an  error  dialog  is
     displayed,  enabling  you to locate the offending garden and
     lines.

BBBBUUUUGGGGSSSS
     The visual class name conversion is performed by a  standard
     toolkit  routine.  It  accepts  only  American spelling, the
     English spelling of 'grey' and 'colour' are not allowed.

     The Drag widget should perhaps just be  a  shell,  having  a
     single child of Icon, to do the rendering.

     The PixmapList widget should perhaps be a  composite  widget
     with  Icon  children.  However when I tried this using a Box
     widget, it didn't work with the insert function.

CCCCOOOOPPPPYYYYRRRRIIIIGGGGHHHHTTTT
     Copyright (C) 1993 Nathan Sidwell.

AAAAUUUUTTTTHHHHOOOORRRR
     Nathan            Sidwell            <nathan@pact.srf.ac.uk>
     <http://www.pact.srf.ac.uk/~nathan/>

     Additional sprites by Stefan Gustavson <stefang@isy.liu.se>































X Version 11      Last change: 20 December 1993                 8



