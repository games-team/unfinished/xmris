. Copyright (C) 1995, 1994, 1993, 1992 Nathan Sidwell
. RCS $Id: xmris.man 1.3 Tue, 16 Mar 1999 11:28:16 +0000 nathan $
.TH XMRIS 6 "12 December 1995" "X Version 11"
.IX xmris#(n) "" "\fLxmris\fP(n)"
.SH NAME
xmris - video game for X
.SH SYNOPSIS
.B xmris
[-option ...] [-toolkitoption ...]
.LP
.B xmsit
[-option ...] [-toolkitoption ...]
.IX xmris#(n) "" "\fLxmris\fP(n) \(em video game"
.SH DESCRIPTION
.PP
Mr Is is a version of the Mr Do video arcade game for the X Window
System.
.PP
You control a gnome, who can walk around a garden, along paths already
marked, or create new paths wherever you wish. You also have a ball,
which can be thrown in the direction you're facing, towards the gnome's
feet. Points are scored for collecting cherries (if you collect eight
cherries without stopping or pushing an apple, you get a bonus),
killing monsters (by
squashing them, or throwing the ball at them), and collecting the prize
left when all the monsters have come out of their den.
.PP
Extra lives are obtained by killing all the 'EXTRA' monsters at the top
of the garden, so that the letters change from stippled to solid
(or grey to black or white, for colour). One of these comes out
on its own every 5000 points.
When you collect the prize, the normal
monsters freeze, and an extra monster emerges, along with three drones.
Killing the letter monster will kill the drones too. When
the three drones are dead, the normal monsters wake up and things go
faster. When all the normal monsters are killed, or all the
cherries collected, or you have got the final extra monster, you advance
to the next garden.
.PP
You can kill the monsters by throwing the ball at them, or dropping the
apples on them. You get more points for squashing them, and the more you
squash in one go, the more points you get. The extra monster, and its
drones, can eat the apples, provided that they're walking towards the apple.
You die by colliding with a monster (unless its eating an apple, in
which case no harm is done), or by being squashed by a falling apple.
Sometimes a falling apple will break open to reveal a diamond. The points
scores are scaled by the game speed, (see below).
.PP
Your score may be immortalized in the all time best scores and/or the
best of the day scores, and/or your own personal best scores. If your
score was added to the best of the day
after 21:00, it is kept until noon the next day, otherwise it will be
removed at midnight. There is only one entry per user in the all time
best and the best of the day tables.
.PP
There are two load lines at the bottom edge of the window. One shows the
frame time ratio and grows from left to right. The other shows the
frame loading and grows from right to left. Note that these two lines can
overlap, and are drawn with xor plotting. You can tell which is which,
because the frame loading line alters on a frame by frame basis, whereas
the frame time ratio only alters occasionally. The frame load line grows by
one pixel for every frame which took longer to animate than there was
allotted time, and is shrunk by one pixel for each frame which is animated
in time. The frame time ratio shows the actual frame time, relative to the
the ideal frame time. For a frame time ratio of r, the line is 1 - 1 / r
the width of the window. Ie, for frame time ratio of 3 (one third speed) it
covers two thirds of the window width. The frame time ratio is a long
time average of the real frame times. It is used to scale the points
scored in the game. The higher the ratio, the lower the score, thus making
heterogeneous comparisons possible. The score scaling is biased towards
lower frame ratios, so you can't get a higher score just by making the game
slower. If your system becomes heavily loaded, you can pause the game, to
prevent the frame time ratio being updated. When the frame load line
diminishes, you can resume the game.
.PP
Because an interrupt is used to control the frame rate, the
animation is reasonably smooth. Though sometimes busywaiting will be
needed to get the best results. The game works best with all other
processes asleep. If another process
gets too much processor time, the animation will be jerky, and the load
line will start to grow.
.PP
You probably want to position the pointer at the bottom of the window, so
that it doesn't interfere with the play area. You'll notice it flicker,
if one of the sprites moves under it.
.PP
The game is controlled from the keyboard. All the key bindings can be
changed by the toolkit application resource mechanism, or during
one of the demonstration screens. There are four direction keys, known
as up, down, left and right and the ball can be thrown with the throw key.
Because the paths are aligned to a matrix, it is only possible
to go in any direction at intersections. Elsewhere you can either go
horizontally or go vertically.
Pressing more than one direction key will turn the gnome appropriately
at the next intersection, so you can go round corners by pressing the
new direction key before releasing the old one. If you press a single
direction key to go in an impossible direction (ie not at an intersection),
the gnome will either continue in the direction it was already going, or,
if stationary, move towards the nearest
intersection. As an example, suppose you're going left and
want to go up at the next intersection, the sequence would be,
.br
.ne 4
.IP
left pressed, because that's the way you're going
.br
up pressed, before the intersection
.br
left released, when you've gone round the corner
.br
.PP
The game can be paused by iconizing it with the iconize key (when your
boss walks in), or by losing the keyboard focus, or by
pressing the pause key. When de-iconized, the game remains paused.
To continue, press the throw key. When paused, you can abort the current
game by pressing the quit key. If the game is
displaying the demonstration screens, the quit key will quit the game,
and pause key will cycle onto the next demonstration screen. During the
score table display, the direction keys can be used to change to a
different score table. Up or right cycle forwards and down or left
cycle backwards. During the garden demonstration, the direction keys
can be used to select a different garden. If you start the game
from that new garden, you will start at that level, but not score
anything. During the
game there are several information screens and pauses, these can be
skipped by pressing the throw key.
.PP
The keys can be changed by using the keyboard key. Each logical key name
is prompted for, and you can select a new key binding by pressing the
one you want. Pressing the throw key will keep the binding for that
particular key (remember the throw key may change half way through this
process). You cannot map one key onto two functions, Mr Is will
wait until you give an unambiguous set of keys. Key bindings set this
way will be forgotten when Mr Is terminates. To permanently set the
key bindings, you will have to the the X resources.
.PP
Mr Is will use colour sprites, if the visual permits it (has a colour map
size of more than 15, and you haven't forced monochrome). All the colours
bar black and white are user definable. There are four sets, one for
each of the four combinations of gender and swap flag. The
colours are allocated in reverse order of their distance in colour
space, from currently allocated colours (the most distant colours are
allocated first). That way, if allocation fails because the colour map
is full, an allocated substitute colour, which is nearest the desired
colour, can be used and the allocated colours are kept maximally distant.
You can limit the number of distinct colours with the -distinct option.
A warning message is sent to stderr, if a colour allocation fails. The
-colours argument shows how these are allocated, and -help -colours can
be used to get the colour resource names.
.SH OPTIONS
Mr Is accepts the standard X Toolkit options, as well as these.
.br
.ne 3
.TP
.B \-help
Lists the command options, application resource fields and some other
information to stderr. Does not start the game. If the -colours option is
supplied too, then the colour resource classes are listed instead, with
their default values. The format of this list is suitable for inclusion
in a resource file. Note, this does not list out the colour values that
you would get if you ran the game, as it does not read the color
resources.
.br
.ne 5
.TP
.PD 0
.B \-swap
.TP
.B \-rv
.TP
.B \-reverse
.PD
Normally the foreground is black and the background white, this swaps
them round. On colour systems, this may also alter other colours. 
.br
.ne 4
.TP
.PD 0
.B \+swap
.TP
.B \-noswap
.PD
Override a swap resource in your resources, to force unswapped colours.
.br
.ne 3
.TP
.B \-mono
Use black and white, even on colour displays. (Unfortunately, the obvious
option, '-bw', is already nabbed by the toolkit as borderwidth.)
.br
.ne 4
.TP
.PD 0
.B \-random
.TP
.B \+random
.PD
Mr Is has two methods for placing the apples. They will either be placed
according to one of four sets of explicit apple positions, or placed randomly
on any permitted location (though trying not to place them adjacently). These
two options override the default set by the resources, -random places them
randomly, and +random uses one of the four sets.
.br
.ne 5
.TP
.PD 0
.B \-mris
.TP
.B \-msit
.TP
.B \-gender \fIgender\fP
.PD
Mr Is can also be run as xmsit. The two sexes have different sprites.
Mris selects the classic sprites, while msit selects a more modern set.
The gender of the game is taken from the program name (mris or msit)
but may be overridden by these two switches. Valid genders
are 'he', 'she', 'female', 'male', 'msit', 'mris', 'boy', 'girl'.
The game is known as xmris (eks mister iz), because the arcade game was masculine.
.br
.ne 3
.TP
.B -busywait
Forces the game timing to be done by busy waiting, rather than with an
alarm timeout. Some systems have particularly inaccurate alarms, and
this option may improve things, by not using the system's timer signal at
all. Some alarms go off before the requested
time. Mr Is will detect this, and insert a busy wait in the remaining
time. A warning will be displayed as well.
Note that this is different to forcing busywaiting, as the timer signal
is still being used for the initial part of the frame delay.
.br
.ne 3
.TP
.B \-dir \fIscore-directory\fP
Specify a different score directory.
.br
.ne 4
.TP
.PD 0
.B \-username
.TP
.B \-realname
.PD
The name for the score file can be either the username or the real name.
These options select which to use. The default is to use the real name.
If the real name is unobtainable, the username will
be used anyway. If the current score file has an entry by the other
name, then it will be changed to the new name.
.br
.ne 3
.TP
.B \-gardens \fIgarden-file\fP
Specify a garden definition file. This allows you to alter the initial
garden layouts. The file is searched for in the current directory, and
the Mr Is subdirectory of app-defaults. These are explained below.
.br
.ne 3
.TP
.B \-scores
List the high scores to stdout. Does not start the game. Note that this
will still need to open an X display, in order to read the X resources
(which may affect the score directory). If you don't want the defaults
read, use the +display option too.
.br
.ne 4
.TP
.PD 0
.B +display
.TP
.B -nodisplay
.PD
Inhibits the opening of an X display. This option may only be used with the
-scores, -expire or -remove options. Note that the X resources may override
the default score directory, and that this will not be done -- you will have
to use the -dir option too.
.br
.ne 3
.TP
.B \-remove \fIname\fP
Allows the game's owner to remove someone's scores. The option will only
work when the real and effective user ids are the same. (ie
a setuid Mr Is has been run by its owner). After updating the files,
the score tables are listed, and the game does not start. An X display
will still need to be opened, to read the X resources, which may override
the default score directory. The +display option may be used to prevent
this.
.br
.ne 3
.TP
.B \-expire \fIdate\fP
Allows you to remove your own scores before or after a certain date.
If your high score is removed, then it is replaced
with a new personal high score. After updating the files, the score tables
are listed, and the game does not start. An X display will still need to
be opened, to read the X resources, which may override the default score
directory. The +display option may be used to prevent this.
.IP
The date format is very flexible. Either an absolute or a relative date
may be given. Both may be prefixed with a '+' or '-'. These have opposite
interpretations for relative and absolute dates. For an absolute date a '+'
will delete those after the date and a '-' will delete those before. The
default is to delete those before. For a 
relative date a '+' will delete those older than specified, whereas a '-'
will delete those younger. The default is to delete those older.
.IP
Relative dates are given as a number with an
optional trailing modifier. A modifier
of 'years', 'months', 'weeks', 'days', 'hours', 'minutes'
or 'seconds' can be used to scale the number appropriately (there are 365.25
days a year and 30.5 days a month). The modifier may be abbreviated, but
there cannot be any spaces between the number and modifier.
.IP
Absolute dates are given as three fields separated by any
non-alphanumeric character, or by a change from numerals to letters. The
month may be entered numerically or
named (abbreviated or not). The day, month and year fields may be in any
order, their values are used to infer which is which. The year may be
the full year name (eg 1993), or just the last two digits, those in the
range 00 to 89 are in the 21st century, and those from 90 to 99 are the
20th century. If the date is ambiguous (eg 1/1/1), a stored format is used
and a warning is given. The following are valid unambiguous dates, '11jun93'
(dmy), '14/3/93' (dmy), 'april6:93' (mdy), and '0-16-8' (ydm).
.br
.ne 3
.TP
.B \-format \fIformat\fP
Allows the game's owner to set the date format which is stored with the
score file, for future use disambiguating dates. The format is
automatically stored if none is set. The format must be a three character
string containing one each of 'D', 'M' and 'Y'.
.br
.ne 3
.TP
.B \-depth \fIdepth\fP
Mr Is will use the default depth of the screen. You may wish to override
that by using this option. Selecting a different depth may affect the
visual selected.
.br
.ne 3
.TP
.B \-visual \fIvisual-class\fP
Mr Is will pick the default visual for the depth chosen, but you can override
that by specifying a particular visual class. Valid visuals
are 'PseudoColor', 'DirectColor', 'TrueColor', 'StaticColor', 'GrayScale',
and 'StaticGray'. To see which one is picked, you can use the -colours
option. If you do select a non-default visual, you may have to specify
a private colour map too, due to limitations of the server or display.
.br
.ne 3
.TP
.B \-private
This forces Mr Is to allocate a private colour map. Normally Mr Is will
share the default colour map of the selected visual, but if that does not
have enough free colour cells then some colours will have to be shared.
.br
.ne 4
.TP
.PD 0
.B \-colours
.TP
.B \-colors
.PD
Show how the colours are allocated, and which visual has been selected.
The allocation is listed to stdout. When allocating each colour,
its resource name and rgb values are listed together with the nearest
already allocated colour and the distance between them in colour
space. The allocated pixel number is printed last. If given with the -help
option, the colour resource classes are listed, and the game does not start.
.br
.ne 3
.TP
.B \-distinct \fIn\fP
Sets the number of distinct colours used. This can be used to limit the
number of colours used from the colour map. Black and white are not included,
and neither are the two writable colours used for the garden backgrounds
on dynamic visuals. Note that
-distinct 0 is different from -mono, even though both will only use black and
white.
.br
.ne 3
.TP
.B \-static
Do not use dynamic background colours, even if the visual supports them. The
default uses two dynamic colours, which alter during the game.
.br
.ne 3
.TP
.B \-sprites
Show all the sprites during the demonstration cycle. This can be
used when
you are defining your own sprite colours. The direction keys will control
the direction in which the demonstration animated sprites face, and the
throw key will cycle the background colours for pseudo colour visuals.
.SH APPLICATION RESOURCES
Mr Is uses the X toolkit application resource mechanism for setting up
the environment. Application resource items start with 'Xmris'. The
resource name can be derived from the given resource class by
decapitalizing it. For example 'cherryStalk' is the resource name for the
class 'cherryStalk'. The following classes are used (choices in '{}' and
defaults in '[]'.)
.br
.ne 11
.TP
.B Xmris.Up: \fIkeysym\fP [apostrophe]
.PD 0
.TP
.B Xmris.Down: \fIkeysym\fP [slash]
.TP
.B Xmris.Left: \fIkeysym\fP [z]
.TP
.B Xmris.Right: \fIkeysym\fP [x]
.TP
.B Xmris.Throw: \fIkeysym\fP [space]
.TP
.B Xmris.Pause: \fIkeysym\fP [p]
.TP
.B Xmris.Quit: \fIkeysym\fP [q]
.TP
.B Xmris.Iconize: \fIkeysym\fP [i]
.TP
.B Xmris.Keyboard: \fIkeysym\fP [k]
.PD
These give the logical key bindings. If the key symbol is unknown, the
default will be used, and a warning printed. Note that these are case
sensitive.
.br
.ne 3
.TP
.B Xmris.Dir: \fIscore-directory\fP
The name of the high score directory.
.br
.ne 3
.TP
.B Xmris.UserName: \fI{yes, no}\fP [no]
Selects whether the username or real name should be used for your entry
in the high score table.
.br
.ne 3
.TP
.B Xmris.Gardens: \fIgardens-file\fP
The name of the garden definition file.
.br
.ne 3
.TP
.B Xmris.ReverseVideo: \fI{yes, no}\fP [no]
Specifies whether to use swapped colours or not.
.br
.ne 3
.TP
.B Xmris.Mono: \fI{yes, no}\fP [no]
Whether the default is for monochrome on colour displays.
.br
.ne 3
.TP
.B Xmris.Random: \fI{yes, no}\fP [no]
Sets whether the apples are placed randomly or not.
.br
.ne 3
.TP
.B Xmris.Gender: \fIgender\fP [he]
Sets the default game gender. Valid genders
are 'mris', 'msit', 'she', 'he', 'female', 'male', 'boy', 'girl'.
.br
.ne 3
.TP
.B Xmris.Busywait: \fI{yes, no}\fP [no]
Determines whether the game timing is always done by busy waiting.
.br
.ne 3
.TP
.B Xmris.Depth: \fIdepth\fP
Set the required screen depth to use.
.br
.ne 3
.TP
.B Xmris.Visual: \fIvisual-class\fP
Set the required visual class to use. Valid visuals
are 'PseudoColor', 'DirectColor', 'TrueColor', 'StaticColor', 'GrayScale',
and 'StaticGray'.
.br
.ne 3
.TP
.B Xmris.Private: \fI{yes, no}\fP [no]
Set whether or not to use a private colour map.
.br
.ne 3
.TP
.B Xmris.Distinct: \fIn\fP
Set the number of distinct colours allocated from the colour map.
.br
.ne 3
.TP
.B Xmris.Static: \fI{yes, no}\fP [no]
Do not use dynamic background colors.
.PP
For example, if you want to use the arrow keys, the following will work
.br
.ne 6
.IP
Xmris.Up:       Up
.br
Xmris.Down:     Down
.br
Xmris.Left:     Left
.br
Xmris.Right:    Right
.br
.PP
In addition, you have the normal resources such as '*Font'.
.PP
Normally the cursor is invisible in the Mr Is window. You can force
a cursor to be shown by setting the "Xmris*cursorName" resource to a
named cursor.
.SH COLOUR RESOURCES
There are many colour name defaults. You can specify different ones for
the four combinations of gender and swap resources, or use the same for some
combinations. There is no reason why all these cannot be different colours,
but note that the more unique colours you define, the more colour map entries
you will use up. The colours black and white are already known about, but
because of the way X parses hex colour names, I have programmed white as
#FF00FF00FF00 (what #FFFFFF expands to), not #FFFFFFFFFFFF (what I think
#FFFFFF should expand to). This means that if you specify a white colour to
more than 8 bit accuracy, a new colour will be allocated. (This is a bug.)
Of course, you can specify the colours by name ('NavajoWhite'), so long
as X can grok it by searching your colour database.
.PP
Most of the sprites have a black edge to them on the unswapped colour scheme,
this gives comic like sprites. This edge is not included for the swap colour
scheme, and the sprite's colours go right up to the sprite's edge.
Most of the sprites will be surrounded by a halo of the
background colour, so that they don't blend in with each other, when crossing.
Another thing to watch out is contrast compensation.
Because of eye physiology, colours can look different, depending on the
surrounding colours, and light colours
look brighter on dark backgrounds than they do on light ones. A particular
case of the former is if pink is used for the player's face. On white
backgrounds pink looks alright, but on dark backgrounds the pink can look
quite brown, and must be brightened up, if you still want it to look pink.
The latter effect means that the blue used for the drones is bright for a
dark background and darker for a light background. There is no requirement
that those colours with a specific colour in their name, need actually be
a shade of that colour. For example GreenBack could be #A020F0 (purple).
You can use the -sprites and -colours options to check out how these colours
have been defined and look, and the distinct resource to limit the
distinct colours used.
.PP
The colour resources use the 'mris' or 'msit' widget instance within the
widget tree. They have the optional sub resource 'swap'.
The following are valid.
.br
.ne 9
.IP
Xmris*Background:               for all
.br
Xmris*mris*Background:          for all mris
.br
Xmris*mris.swap.Background:     for swapped mris
.br
Xmris*mris.Background:          for unswapped mris
.br
Xmris*msit*Background:          for all msit
.br
Xmris*msit.swap.Background:     for swapped msit
.br
Xmris*swap.Background:          for all swapped
.br
.PP
The usual toolkit parsing rules apply to these resources. Namely that '*'
is used to fill out levels of hierarchy, while '.' is used for explicit
matching. The toolkit uses the longest matching string to select resources
in the case of ambiguities. Ie, 'Xmris*Swap.Background' will be selected
over 'Xmris*Background' for the swapped versions.
.PP
The defaults for 'mris', 'mris.swap', 'msit' and 'msit.swap' are included
after the resource class.
.br
.ne 4
.TP
.PD 0
.B Background       [#FFFFFF, #000000, #FFFFFF, #000000]
.TP
.B Foreground       [#000000, #FFFFFF, #000000, #FFFFFF]
.TP
.B BorderColor      [#000000, #FFFFFF, #000000, #FFFFFF]
.PD
The main foreground, background and border colours. The foreground colou
is used
for all text and on garden scoring. The background is used for the pathways
and non-garden parts of the screen. The border color is used for the board
partion lines.
.br
.ne 10
.TP
.PD 0
.B GreenBack        [#77BB77, #BBFFBB, #77BB77, #BBFFBB]
.TP
.B GreenFore        [#007700, #00BB00, #007700, #00BB00]
.TP
.B RedBack          [#BB7777, #FFBBBB, #BB7777, #FFBBBB]
.TP
.B RedFore          [#770000, #BB0000, #770000, #BB0000]
.TP
.B BlueBack         [#7777BB, #BBBBFF, #7777BB, #BBBBFF]
.TP
.B BlueFore         [#000077, #0000BB, #000077, #0000BB]
.TP
.B DroneBack        [#AA3333, #FF6666,  ------,  ------]
.TP
.B DroneFore        [#992222, #FF2222,  ------,  ------]
.PD
These are the colours used for the hedges. Two are used per garden.
For pseudo colour visuals, droneback and dronefore are used when the prize
is eaten.
.br
.ne 3
.TP
.B Ball             [#FFFF77, #FFFF77, #FF00FF, #FF00FF]
This is the ball colour.
.br
.ne 4
.TP
.PD 0
.B CherryRed        [#EE0000, #EE0000, #EE0000, #EE0000]
.TP
.B CherryStalk      [ ------, #EEAA66,  ------, #EEAA66]
.PD
The cherries use two colours, one for the fruit and the other 
for the stalk. The cherry's glint is always white.
.br
.ne 5
.TP
.PD 0
.B Apple1           [#EEDD00, #EEDD00, #EEDD00, #EEDD00]
.TP
.B Apple2           [#DD3300, #DD3300, #DD3300, #DD3300]
.TP
.B AppleFaint       [#BBBBBB,  ------, #BBBBBB,  ------]
.PD
The apples use two colours for their skin. The apple's flesh and
glint is always white.
.br
.ne 4
.TP
.PD 0
.B Gem1              [#DDDDDD, #DDDDDD, #DDDDDD, #DDDDDD]
.TP
.B Gem2              [#BBBBBB, #BBBBBB, #BBBBBB, #BBBBBB]
.PD
The gem facets are white or one of the two gem colours. The lines
between them are black and the sparkle is black for the unswapped scheme
and white colour for the swap scheme.
.br
.ne 4
.TP
.PD 0
.B LetterGot        [#000000, #FFFFFF, #000000, #FFFFFF]
.TP
.B LetterNotGot     [#BBBBBB, #BBBBBB, #BBBBBB, #BBBBBB]
.PD
The extra letters and game title lettering uses two colours. One to show
letters which have been got, one for those which have not been got. They
do not have an edge colour put around them.
.br
.ne 11
.TP
.PD 0
.B Normal           [#EE0000, #EE0000, #EE0000, #EE0000]
.TP
.B Munch1           [#FFFF00, #FFFF00, #FFCC00, #FFCC00]
.TP
.B Munch2           [#CCCCCC, #CCCCCC, #FFCC00, #FFCC00]
.TP
.B Drone            [#0000DD, #6666FF, #00FF00, #00FF00]
.TP
.B DroneTongue      [ ------,  ------, #EE0000, #EE0000]
.TP
.B Extra            [#EEFF00, #EEFF00, #EEFF00, #EEFF00]
.TP
.B Chomp            [#FFFFFF, #FFFFFF, #CCFF00, #CCFF00]
.TP
.B ChompLip         [#77FFFF, #77FFFF,  ------,  ------]
.TP
.B ChompTongue      [ ------,  ------, #EE0000, #EE0000]
.PD
Most of the monsters have only one additional colour (to black and white),
but in some instances there are additional colours for the features implied
by the resource name.
.br
.ne 6
.TP
.PD 0
.B Player           [#0000DD, #6666FF, #6666FF, #6666FF]
.TP
.B PlayerBobble     [#FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF]
.TP
.B PlayerSkin       [#FFCCCC, #FFDDDD, #FFCCCC, #FFDDDD]
.TP
.B PlayerBoot       [ ------, #EEAA66, #773322, #DD9955]
.PD
The player uses four additional colours.
The bobble colour is also used for the flecks
in the player's suit. The skin colour is used for the face and hands.
.br
.ne 3
.TP
.PD 0
.B Seat             [#EE0000, #EE0000, #EE0000, #EE0000]
.PD
The little seat on which you can rest uses this additional colour.
.br
.ne 5
.TP
.PD 0
.B Cake             [#FFFF77, #FFFF77, #FFFF77, #FFFF77]
.TP
.B CakeIcing        [#DD9955, #EEAA66, #DD9955, #EEAA66]
.TP
.B CakeJam          [#EE0000, #EE0000, #EE0000, #EE0000]
.PD
The cake prize has an icing layer and a jam layer around the cake layers.
.br
.ne 4
.TP
.PD 0
.B Spanner          [#AAAAAA, #DDDDDD, #AAAAAA, #DDDDDD]
.TP
.B SpannerShadow    [#000000, #000000, #000000, #000000]
.PD
The spanner prize only uses these two colours.
.br
.ne 5
.TP
.PD 0
.B Brolly1          [#FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF]
.TP
.B Brolly2          [#EE0000, #EE0000, #EE0000, #EE0000]
.TP
.B BrollyHandle     [#DD9955, #EEAA66, #DD9955, #EEAA66]
.PD
The umbrella prize uses four colours. The edge colour is used to demark
the parasol colour areas.
.br
.ne 4
.TP
.PD 0
.B MushroomStalk    [#FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF]
.TP
.B MushroomCap      [#EE0000, #EE0000, #EE0000, #EE0000]
.PD
The mushroom prize uses these two additional colours.
.br
.ne 5
.TP
.PD 0
.B ClockFace        [#FFFFFF, #FFFFFF, #FFFFFF, #FFFFFF]
.TP
.B ClockBell        [#00DD00, #00DD00, #00DD00, #00DD00]
.TP
.B ClockRim         [#0000DD, #00DD00, #00DD00, #00DD00]
.PD
The clock prize uses these thee additional colours.
.SH GARDENS
You may override the default garden layouts by specifying a garden
file. The file is a text file consisting a of a set of garden definitions,
include files, or comments. An include file is specified
by '#include "\fIfilename\fP"',
where filename is the name of the included file. Relative include
files are searched for relative to the including file, or, if
that fails, in the Mr Is subdirectory of app-defaults directory (this
will usually be '/usr/lib/X11/app-defaults/xmris'). In
If the filename is null, then the internal
gardens will be included. Comments are delimited by '/*' and '*/',
A garden definition is the following
format, '{\fIfillpattern\fP, \fIbackgroundcolour\fP,
\fIapples\fP, {\fIlayout\fP}},'. New gardens must begin on a new
line.
\fIFillpattern\fP is an integer specifying one of the following fill
patterns,
.br
.ne 6
.IP
0       brickwork
.br
1       diagonal stripes
.br
2       cross hatched
.br
3       zigzag lines
.br
.PP
\fIBackgroundcolour\fP is an integer specifying one of the following
background colour schemes,
.br
.ne 5
.IP
0       red
.br
1       green
.br
2       blue
.br
.PP
\fIApples\fP specifies the number of apples to place in the garden.
Its upper limit is twelve.
\fILayout\fP consists of 13 strings of 12 characters each, such
as '"..b@@B..@@.B"'. Within these strings the following characters
are used,
.br
.ne 10
.IP
A-D     Blank path
.br
E-H     Cherry on path
.br
I-L     Den on path
.br
MP      Player on path
.br
@       Cherry on background
.br
a-p     Explicit apple possibilities
.br
+       Invalid apple location
.br
\&.       Background
.br
.PP
The path characters specify connections to the cell below and to the right.
A bit mask is obtained by subtracting the base character.
Bit 0 connects downwards and bit 2 connects to the right.
The explicit apple positions define four sets of apple locations, using
the four bits obtained by subtracting the base character.
Random apples will be placed on any cell which is not a pathway, cherry or
invalid apple location.
.PP
There must be at least one cherry, at least one den and exactly one player.
Certain locations must be pathway. The garden is checked, and if faulty,
it will be fixed, if possible.
.PP
The format of these garden files is similar to a C source file, except that
includes and comments can only occur between garden definitions, and missing
or superfluous commas are ignored.
.PP
You can examine the gardens during the demonstration screens. When a
garden is shown, the direction keys can be used to change the garden
number. Up and down increment and decrement by ten, whilst right and
left increment and decrement by one. If a game is started from the
selected garden, then it will be the initial garden, but you won't score
any points.
.SH ENVIRONMENT
A few environment variables are used to locate resources.
.br
.ne 3
.TP 4
.B DISPLAY
The default display to connect to. May be overridden with the -display
option.
.br
.ne 3
.TP 4
.B LOGNAME, USER, HOME
Read to determine the name to use for the score tables, and the
user's home directory, if getpwuid(3) fails.
.SH FILES
The loadable garden file must be fully named, or located in the
score directory. They may have any name. The score files have the
following names.
.br
.ne 3
.TP
.B .../xmris.score
The high score file. The directory is system dependent, and may be
changed by application resource or option.
This file must either exist and be writable to by Mr Is, or the
directory containing it must be writable by Mr Is. This
can be done by chmoding the score file or directory, or by
setuiding Mr Is appropriately. A non-setuid Mr Is will chmod the score
file to 666 if creating it.
.br
.ne 3
.TP
.B .../xmris.lock
In some systems, where lockf or flock doesn't work, this temporary file
is used to perform the locking. The directory containing it must be
writable by Mr Is. This can be done by chmoding the directory, or
by setuiding Mr Is appropriately.
.br
.ne 4
.TP
.PD 0
.B .../xmris-<name>
.TP
.B ~/.xmris.score
.PD
One of these files is used to store the personal best scores.
Mr Is first looks for the personal score file in the score directory and
then in the home directory. If a personal
score file cannot be found, Mr Is attempts to create one. 
If the file is found in the user's home directory, Mr Is attempts
to move it to the score directory. Mr Is
will attempt to create the personal files in the score directory. If this
cannot be done, the personal score file is placed in the home directory. In
order to create the personal score file in the score directory, Mr Is will
have to have the correct access rights, as with the lock file. A setuid Mr Is
will juggle the effective uid correctly for accessing both the score directory
and the users' home directories.
.br
.ne 5
.TP
.PD 0
.B ~/.Xdefaults
.TP
.B .../app-defaults/Xmris.ad
.PD
You can place you favourite key bindings and stuff in an application
resource file, and Mr Is will use them, rather than its compiled defaults.
See X for information about how these are searched.
.br
.ne 6
.TP
.PD 0
.B .../app-defaults/xmris/<gardens>
.TP
.B <score-dir>/<gardens>
.TP
.B <score-dir>/gardens/<gardens>
.PD
Search path for loadable gardens. The suffix ".gdn" is appended to the
filename, if required.
.br
.ne 3
.TP
.B digits.gdn, alphabet.gdn, puzzle.gdn
Some of the garden files.
.SH SEE ALSO
.BR xmred (6)
.SH ERRORS
.PP
If you use a lock file, rather than lockf, and an error occurs
creating the lock file, a message is printed on stderr, and file
locking is not done for that access. Subsequent accesses may be alright.
.PP
If an error occurs opening the score file, a message is printed on
stderr, and the score file is disabled. Personal score files will
be generated in the users' home directories.
.PP
Various errors can occur during initialization, most are obvious. Note
that if you requested a non-default visual, you may also have to request
a private colormap, otherwise an X error 8 on request 1:0 occurs when
realizing the widget.
.PP
Some systems' timer returns too soon. Mr Is detects this, and then starts
performing a busywait at the end of the timer period. A warning is also
printed. If this is the case, it may be better to force busywaiting with
the busywait resource.
.PP
If a loadable garden is incorrect, an error is displayed, enabling you
to locate the offending files and lines. The garden is ignored.
.SH BUGS
.PP
Mr Is can be addictive, so don't blame me if your work suffers.
.PP
Mr Is does not check that the key definitions in the application
resources do not conflict with each other. Neither are the colours
checked, to see that things are actually visible.
.PP
Some of the -msit -swap sprites have black pixels at their edge.
These should really be background colour pixels, but this is only
significant if the -swap background colour is not dark.
.PP
Best of the day scores scored between 21:00 Dec 31 and 00:00 Jan 1
won't be kept until noon on New Year's Day.
.PP
One of the sprites with lettering, has the lettering reversed when
facing left.
.PP
Getting accurate, stable timing is difficult, as Unix is not a
real time OS. Unix schedules processes in ticks, with a certain
granularity. Getting finer grained timing than that is very much
system dependent. There is also slippage between receiving
one interrupt and starting the next one. You don't want to get the
interrupt to restart itself (even though this is possible), as
you then get very rude behaviour if your main loop is a bit too slow,
(Mr Is on speed). Some timers round downwards, returning before the
requested time. This has to be detected, and a busy wait inserted.
.PP
The visual class name conversion is performed by a standard toolkit
routine. It accepts only American spelling, the English spelling of
'grey' and 'colour' are not allowed.
.SH COPYRIGHT
Copyright (C) 1995, 1994, 1993, 1992 Nathan Sidwell.
.SH AUTHOR
Nathan Sidwell <nathan@pact.srf.ac.uk> <http://www.pact.srf.ac.uk/~nathan/>

Additional sprites by Stefan Gustavson <stefang@isy.liu.se>
