. Copyright (C) 1993 Nathan Sidwell
. RCS $Id: xmred.man 1.3 Tue, 16 Mar 1999 11:28:16 +0000 nathan $
.TH XMRED 6 "20 December 1993" "X Version 11"
.IX xmred#(n) "" "\fLxmred\fP(n)"
.SH NAME
xmred - garden editor for xmris
.SH SYNOPSIS
.B xmred
[-option ...] [-toolkitoption ...]
.IX xmred#(n) "" "\fLxmred\fP(n) \(em video game editor"
.SH DESCRIPTION
.PP
Mr Ed is a tool for creating and editing alternate gardens for Mr Is.
Hopefully its user interface is intuitive enough that most of this manual
page need not be read by a casual user.
.PP
In the default configuration, the main window presents a garden image on the
left, a control panel on the right and a list of gardens underneath. Gardens
can be loaded and saved using the menu, these then appear in the list below.
The insert option loads a file into the copy buffer. It can then be dragged
to the desired place in the garden list.
.PP
To edit a garden (or comment, or include), either select it with a button
press and release, or with a drag and drop into the edit box at the lower
right. The drag and drop will remove it from the garden list, selection will
copy it, and the source garden will be updated too. To delete gardens,
drag them to the copy buffer.
The main garden display will be updated, if it was a garden being moved to the
edit box. The box on the lower right is a copy buffer. You can drag and drop
into and out of this to the garden list or one of the edit boxes. Similarly
you can drag and drop from the edit boxes to the garden list, and within the
garden list itself. Note that drag and drop always removes the source, if it
is within the garden list, whereas selection does not delete the source.
.PP
The control panel shows a set of options, which can be bound to buttons. This
can be done either by press and release with the required button, or drag and
drop onto the required button's icon. You can bind an option to more than one
button. However, press and release on the apples option will simply
change the currently selected explicit apple, not necessarily bind that option
to a new button. The fill pattern and color can be select by pressing on
them. The current garden will be updated. The list of buttons shows which
options are bound to which buttons. You can also change button bindings by
pressing the destination button with the pointer over the button with the
required option's icon, or by drag and drop from button to button.
.PP
The six options are,
.TP
.B Apple
This selects where apples from one of the four sets of explicit apple
locations are placed. To select which set is being controlled, press on the
relevant quadrant of the icon. One of these sets is used when Mr Is is used
with the +random option. You can place apples anywhere there isn't a path,
even in an unstable location, which will immediately fall. Placing an explicit
apple on a path, will fill the path.
.TP
.B Random
This controls where apples may be placed randomly, if Mr Is is started with
the -random option. Apples will only be placed on the specified locations, or
where ever an explicit apple could be located. Unless you specify otherwise,
random apples may be located anywhere there isn't a path, even unstable
locations which will immediately fall. Placing a random apple on a path, will
fill the path.
.TP
.B Cherry
This controls where cherries are located. There must be at least one cherry
on a garden. Cherries may be located anywhere on the board.
.TP
.B Path
This controls where the initial path is. Each location on the board consists
of a centre and four edges. Depending on the exact location of the pointer,
you will select either the centre, or an edge. Filling a path will place a
random apple in the filled location. The four locations immediately below the
'EXTRA' letters must be clear.
.TP
.B Player
This controls where the player starts from. There must be exactly one
such location.
.TP
.B Den
This controls where the monsters start from. There must be at least one
den.
.PP
The list of totals show the counts for the explicit apples, cherries, random
apples required, apple spaces, unstable apple positions, dens and player
positions. A warning is shown on the left of any which are inconsistant, or
out of range. The number of apples for a garden can be set by moving the
scroll bar at the left of the apple icon. Note that when you change the number
of apples, or add or remove explicit apples, a warning will change on on
some or all of the explicit apple counts. This is just to remind you that
you must do some more work on the garden, before it is consistant. There are
two types of random apple spaces, stable and unstable. The unstable space
count is shown with an arrow in it, the other apple space count shows the
total number of apple spaces. The hazzard warning on the unstable count, just
shows that you have some unstable apple positions (this may be intentional on
your part). The hazzard on the total apple space count indicates that there
are less spaces than the number of apples you specified for the garden.
.PP
Below this is a comment box for the garden. Selecting this will pop up a
dialog which you can enter information about the garden.
.PP
At the lower right of the control panel is a display mode selector. There
are three display modes. The first shows all the explicit apple positions,
4 to a cell when required. The second shows only one set of explicit apples,
the set selected using the apples option quadrant. The final mode shows none
of the explicit apples, just the random apple spaces.
.PP
The garden display is on the left of this. It shows the currently edited
garden. Clicking or dragging a mouse button on this area will perform the
option currently bound to that button. You will notice the totals change when
you do this.
.PP
Mr Ed will use colour sprites, if the visual permits it (has a colour map
size of more than 15, and you haven't forced monochrome). All the colours
bar black and white are user definable. There are four sets, one for
each of the four combinations of gender and swap flag. The
colours are allocated in reverse order of their distance in colour
space, from currently allocated colours (the most distant colours are
allocated first). That way, if allocation fails because the colour map
is full, an allocated substitute colour, which is nearest the desired
colour, can be used and the allocated colours are kept maximally distant.
You can limit the number of distinct colours with the -distinct option.
A warning message is sent to stderr, if a colour allocation fails. The
-colours argument shows how these are allocated, and -help -colours can
be used to get the colour resource names.
.SH OPTIONS
Mr Ed accepts the standard X Toolkit options, as well as these.
.br
.ne 3
.TP
.B \-help
Lists the command options, application resource fields and some other
information to stderr. Does not start the game. If the -colours option is
supplied too, then the colour resource classes are listed instead, with
their default values. The format of this list is suitable for inclusion
in a resource file. Note, this does not list out the colour values that
you would get if you ran the game, as it does not read the color
resources.
.br
.ne 5
.TP
.PD 0
.B \-swap
.TP
.B \-rv
.TP
.B \-reverse
.PD
Normally the foreground is black and the background white, this swaps
them round. On colour systems, this may also alter other colours. 
.br
.ne 4
.TP
.PD 0
.B \+swap
.TP
.B \-noswap
.PD
Override a swap resource in your resources, to force unswapped colours.
.br
.ne 3
.TP
.B \-mono
Use black and white, even on colour displays. (Unfortunately, the obvious
option, '-bw', is already nabbed by the toolkit as borderwidth.)
.br
.ne 5
.TP
.PD 0
.B \-mris
.TP
.B \-msit
.TP
.B \-gender \fIgender\fP
.PD
Mr Ed has two sets of sprites. Mris selects the classic sprites,
while msit selects a more modern set. Valid genders
are 'he', 'she', 'female', 'male', 'msit', 'mris', 'boy', 'girl'.
.br
.ne 3
.TP
.B \-depth \fIdepth\fP
Mr Ed will use the default depth of the screen. You may wish to override
that by using this option. Selecting a different depth may affect the
visual selected.
.br
.ne 3
.TP
.B \-visual \fIvisual-class\fP
Mr Ed will pick the default visual, but you can override
that by specifying a particular visual class. Valid visuals
are 'PseudoColor', 'DirectColor', 'TrueColor', 'StaticColor', 'GrayScale',
and 'StaticGray'. To see which one is picked, you can use the -colours
option. If you do select a non-default visual, you may have to specify
a private colour map too, due to limitations of the server or display.
.br
.ne 3
.TP
.B \-private
This forces Mr Ed to allocate a private colour map. Normally Mr Ed will
share the default colour map of the selected visual, but if that does not
have enough free colour cells then some colours will have to be shared.
.br
.ne 4
.TP
.PD 0
.B \-colours
.TP
.B \-colors
.PD
Show how the colours are allocated, and which visual has been selected.
The allocation is listed to stdout. When allocating each colour,
its resource name and rgb values are listed together with the nearest
already allocated colour and the distance between them in colour
space. The allocated pixel number is printed last. If given with the -help
option, the colour resource classes are listed, and the game does not start.
.br
.ne 3
.TP
.B \-distinct \fIn\fP
Sets the number of distinct colours used. This can be used to limit the
number of colours used from the colour map. Black and white are not included,
and neither are the two writable colours used for the garden backgrounds
on dynamic visuals. Note that
-distinct 0 is different from -mono, even though both will only use black and
white.
.SH RESOURCES
Mr Ed uses the X toolkit application resource mechanism for setting up
the environment. Application resource items start with 'Xmris', so that Mr Ed
will pick up your defaults for Mr Is. The
resource name can be derived from the given resource class by
decapitalizing it. For example 'cherryStalk' is the resource name for the
class 'cherryStalk'. The following classes are used (choices in '{}' and
defaults in '[]'.)
.br
.ne 3
.TP
.B Xmris.ReverseVideo: \fI{yes, no}\fP [no]
Specifies whether to use swapped colours or not.
.br
.ne 3
.TP
.B Xmris.Mono: \fI{yes, no}\fP [no]
Whether the default is for monochrome on colour displays.
.br
.ne 3
.TP
.B Xmris.Gender: \fIgender\fP [he]
Sets the default game gender. Valid genders
are 'mris', 'msit', 'she', 'he', 'female', 'male', 'boy', 'girl'.
.br
.ne 3
.TP
.B Xmris.Depth: \fIdepth\fP
Set the required screen depth to use.
.br
.ne 3
.TP
.B Xmris.Visual: \fIvisual-class\fP
Set the required visual class to use. Valid visuals
are 'PseudoColor', 'DirectColor', 'TrueColor', 'StaticColor', 'GrayScale',
and 'StaticGray'.
.br
.ne 3
.TP
.B Xmris.Private: \fI{yes, no}\fP [no]
Set whether or not to use a private colour map.
.br
.ne 3
.TP
.B Xmris.Distinct: \fIn\fP
Set the number of distinct colours allocated from the colour map.
.PP
In addition, you have the normal resources such as '*Font'.
.SH COLOUR RESOURCES
There are many colour name defaults. For a full description see the xmris(6)
manual page, but note that not all the colors are used for Mr Ed. Provided
that you specified the colour resources for Mr Is loosely enough, Mr Ed
will pick up the same overrides. The foreground color for the Icon widgets
is copied from the apple faint color on color visuals (this is important
for the noswap color scheme).
.SH WIDGET RESOURCES
There are a few resources which are picked up from widgets within the
widget tree. They are the initial button bindings, colors, fills and mode.
The bindings are attached to the individual button displays. The options
are 'apple', 'random', 'cherry', 'path', 'player' and 'den'. The colors, fills
and mode are attached to the color, fill and mode form widgets. The value must
be an integer in the correct range.
.PP
There is an additional composite resource for children of composite widgets
(Paned and SimpleMenu), called 'attach'. This allows you to change the
ordering of sibling widgets. Mr Ed uses this resource to determine the correct
order to create the sibling widgets. For instance, to get the control panel on
the left of the garden widget, use the constraint '*one.garden.attach:panel'.
In addition, the widgets are created in such
an order that Form constraints 'fromHoriz' and 'fromVert' can be
specified in any order.
.PP
There are four new widgets used for Mr Ed, 'Icon', 'Drag', 'Garden'
and 'PixmapList'.
.PP
The Icon widget is a subclass of Simple. It displays a pixmap and allows
its selection with any button press. If a button is dragged on it, it may
invoke a drag widget to perform a drag operation.
It has the following new resources of interest.
.br
.ne 3
.TP
.B dragSensitivity: \fIpixels\fP [4]
Sets the minimum drag which must occur, before a drag widget is popped
up, to take over the dragging. When set to zero, dragging is disabled.
.br
.ne 3
.TP
.B flashDelay: \fIdelay\fP [100]
Sets the time for which the widget is highlit after selection. This time
is in milliseconds.
.br
.ne 3
.TP
.B highlightThickness: \fIpixels\fP [1]
Sets the thickness of the highlight box.
.br
.ne 3
.TP
.B foreground: \fIcolor\fP [default foreground]
Sets the color of the highlight box and block.
.PP
The Drag widget is a subclass of OverrideShell. It displays a pixmap,
and follows the pointer until a button is released. Its use is for drag and
drop. It has no additional resources of user interest.
.PP
The Garden widget is a subclass of Simple. It performs the garden editing.
It has no additional resources of user interest.
.PP
The PixmapList widget is a subclass of Simple. It displays a list of
pixmaps, and permits them to be scrolled. Each pixmap may be selected
by a button press on it, or a drag widget invoked by dragging on a pixmap.
In addition to the Icon widget additional resources, it has the following
additional resources of user interest.
.br
.ne 3
.TP
.B distance: \fIpixels\fP [4]
The distance between each pixmap.
.br
.ne 3
.TP
.B pixmapBorderWidth: \fIpixels\fP [1]
The border width for each pixmap.
.br
.ne 3
.TP
.B pixmapBorderColor: \fIcolor\fP [default foreground]
The border color for each pixmap.
.SH ENVIRONMENT
A few environment variables are used to locate resources.
.br
.ne 3
.TP 4
.B DISPLAY
The default display to connect to. May be overridden with the -display
option.
.SH FILES
.br
.ne 5
.TP
.PD 0
.B ~/.Xdefaults
.TP
.B .../app-defaults/Xmris.ad
.TP
.B .../app-defaults/Xmris-color.ad
.PD
You can place you favourite key bindings and stuff in an application
resource file, and Mr Ed will use them, rather than its compiled defaults.
See X for information about how these are searched.
.br
.ne 3
.TP
.B .../app-defaults/xmris/<gardens>
Search path for loadable gardens used by Mr Is.
.SH SEE ALSO
.BR xmris (6)
.SH ERRORS
.PP
If a loaded garden is incorrect, an error dialog is displayed, enabling you
to locate the offending garden and lines.
.SH BUGS
.PP
The visual class name conversion is performed by a standard toolkit
routine. It accepts only American spelling, the English spelling of 'grey'
and 'colour' are not allowed.
.PP
The Drag widget should perhaps just be a shell, having a single child
of Icon, to do the rendering.
.PP
The PixmapList widget should perhaps be a composite widget with Icon
children. However when I tried this using a Box widget, it didn't work with
the insert function.
.SH COPYRIGHT
Copyright (C) 1993 Nathan Sidwell.
.SH AUTHOR
Nathan Sidwell <nathan@pact.srf.ac.uk> <http://www.pact.srf.ac.uk/~nathan/>

Additional sprites by Stefan Gustavson <stefang@isy.liu.se>
