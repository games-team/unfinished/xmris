/* Copyright (C) 1993, 1992 Nathan Sidwell */
/* RCS $Id: patchlevel.h 1.4 Mon, 22 Nov 1999 14:02:23 +0000 nathan $ */

/* $Format: "#define XMRISVERSION \"V$ReleaseVersion$\""$ */
#define XMRISVERSION "V4.0.5"
/* $Format: "#define XMREDVERSION \"V$ReleaseVersion$\""$ */
#define XMREDVERSION "V4.0.5"
/* $Format: "#define DATE \"$ProjectDate$\""$ */
#define DATE "Mon, 22 Nov 1999 14:02:23 +0000"
